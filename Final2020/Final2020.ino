#include <ros.h>
#include <std_msgs/Empty.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/UInt8MultiArray.h>
#include <Arduino.h>
#include <Servo.h>
#include <AccelStepper.h>
#include <std_msgs/MultiArrayDimension.h>

#define HALFSTEP 8
#define POWER 9
#define DIRECTION 8
#define platform_pin 45
#define RFID_pin 44
#define motor_pin1  10 // IN1 на 1-м драйвере ULN2003
#define motor_pin2  11 // IN2 на 1-м драйвере ULN2003
#define motor_pin3  12 // IN3 на 1-м драйвере ULN2003
#define motor_pin4  13 // IN4 на 1-м драйвере ULN2003
#define front_switch 7
#define back_switch 6
#define HC12_pin 40

class RoverHardware : public ArduinoHardware
{
  public:
  RoverHardware():ArduinoHardware(&Serial1, 115200){};
};

ros::NodeHandle rover_node_handle;
Servo platform_servo;
Servo RFID_servo;
AccelStepper stepper(HALFSTEP, motor_pin1, motor_pin3, motor_pin2, motor_pin4);
std_msgs::String str_msg;
ros::Publisher debugger("debugger", &str_msg);
int stepper_direction = 0;
char buff[32];
bool written = false;

void engine_handler(const std_msgs::Int32 &msg){
  //на вход подается значение от -255 до 255
  //отрицательное - двигатель крутится в одну сторону, положительное - в другую
  //255 - максимальная скорость в обоих направлениях
  
  analogWrite(POWER, msg.data < 0 ? msg.data + 255 : msg.data);
  digitalWrite(DIRECTION, msg.data < 0 ? HIGH : LOW);

  str_msg.data = "engine_handler. Received:";
  debugger.publish( &str_msg );
  itoa(msg.data, buff, 10);
  str_msg.data = buff;
  debugger.publish( &str_msg );
  str_msg.data = "Goes with speed:";
  debugger.publish( &str_msg );
  itoa(msg.data < 0 ? msg.data + 255 : msg.data, buff, 10);
  str_msg.data = buff;
  debugger.publish( &str_msg );
  str_msg.data = "And direction:";
  debugger.publish( &str_msg );
  str_msg.data = msg.data < 0 ? "HIGH" : "LOW";
  debugger.publish( &str_msg );
}

void platform_handler(const std_msgs::Int32 &msg){
  //передается число от 0 до 179 - угол поворота
  //исходное состояние -
  
  platform_servo.write(msg.data);

  str_msg.data = "platform_handler. Received:";
  debugger.publish( &str_msg );
  itoa(msg.data, buff, 10);
  str_msg.data = buff;
  debugger.publish( &str_msg );
}

void RFID_handler(const std_msgs::Int32 &msg){
  //передается число от 0 до 179 - угол поворота
  //состояние, параллельное земле - 0
  
  RFID_servo.write(msg.data);

  str_msg.data = "RFID_handler. Received:";
  debugger.publish( &str_msg );
  itoa(msg.data, buff, 10);
  str_msg.data = buff;
  debugger.publish( &str_msg );
}

void stepper_handler(const std_msgs::Int32 &msg){
  //передается число от -1000 до 1000 - скорость
  //отрицательное - двигатель крутится в одну сторону, положительное - в другую
  //1000 - максимальная скорость в обоих направлениях
  
  stepper_direction = msg.data;
  
  str_msg.data = "stepper_handler. Received:";
  debugger.publish( &str_msg );
  itoa(msg.data, buff, 10);
  str_msg.data = buff;
  debugger.publish( &str_msg );
}

void HC12_handler(const std_msgs::UInt8MultiArray &msg){
  //передается 16 чисел типа byte, которые сразу передаются по радио
  //тестовые значения, чтобы включить маяк: 
  //0xAA,0xFA,0x03,0x14,0x01,0x03,0x75, 0xCA, 0xAA,0xFA,0x03,0x14,0x01,0x03,0x75, 0xCA
  for (int i = 0; i < 16; ++i) {
    Serial3.write(msg.data[i]);
  }
  
  str_msg.data = "HC12_handler. Received:";
  debugger.publish( &str_msg );
  for (int i = 0; i < 16; ++i) {
    itoa(msg.data[i], buff, 10);
    str_msg.data = buff;
    debugger.publish( &str_msg );
  }
}

ros::Subscriber<std_msgs::Int32> engine_sub("engine", &engine_handler);
ros::Subscriber<std_msgs::Int32> platform_sub("platform", &platform_handler);
ros::Subscriber<std_msgs::Int32> RFID_sub("RFID", &RFID_handler);
ros::Subscriber<std_msgs::Int32> stepper_sub("stepper", &stepper_handler);
ros::Subscriber<std_msgs::UInt8MultiArray> HC12_sub("HC12", &HC12_handler);

void setup()
{
  rover_node_handle.initNode();
  rover_node_handle.subscribe(engine_sub);
  rover_node_handle.subscribe(platform_sub);
  rover_node_handle.subscribe(RFID_sub);
  rover_node_handle.subscribe(stepper_sub);
  rover_node_handle.subscribe(HC12_sub);
  rover_node_handle.advertise(debugger);
  pinMode(POWER, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  platform_servo.attach(platform_pin);
  RFID_servo.attach(RFID_pin);
  stepper.setMaxSpeed(1000.0);
  stepper.setAcceleration(10.0);
  stepper.setSpeed(10);
  pinMode(front_switch, INPUT_PULLUP);
  pinMode(back_switch, INPUT_PULLUP);
  
  //настройка радио-модуля
  digitalWrite(HC12_pin, LOW); //перевели в режим приема АТ
  str_msg.data = "HC12 setup:";
  debugger.publish( &str_msg );
  delay(500);
  String ans;
  String HC12_setup[] = {"AT", "AT+V", "AT+DEFAULT", "AT+P8", "AT+C005"};
  for (String at : HC12_setup ) {
    Serial3.println(at); // отправили на hc12
    delay(1000);
    if(Serial3.available() > 0)
    {
      ans = Serial3.readStringUntil('\n');  // читаем ответ
      str_msg.data = ans.c_str();
      debugger.publish( &str_msg );
    }
    delay(500); // эти задержки важны для корректной работы
  }
  digitalWrite(HC12_pin, HIGH); //вернули в обычный режим
}

void loop()
{
  rover_node_handle.spinOnce();
  delay(1);
  if (stepper_direction == 1 && digitalRead(front_switch) == HIGH) { //вперед
    stepper.moveTo(stepper.currentPosition() - 10);
    stepper.run();
    written = false;
  }
  else if (stepper_direction == -1 && digitalRead(back_switch) == HIGH) { //назад
    stepper.moveTo(stepper.currentPosition() + 10);
    stepper.run();
    written = false;
  }
  else if (!written) {
    stepper.stop();
    
    str_msg.data = "Some switch is LOW. Direction is not available.";
    written = true;
    debugger.publish( &str_msg );
  }
}
